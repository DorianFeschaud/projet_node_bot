const url = require('url');

class OpenWeatherMap {
    constructor(appid, units) {
        this._appid = appid;
        this._units = units;
    }

    getWeather(city) {
        const urlow = new url.URL('http://api.openweathermap.org/data/2.5/weather'),
        params = {q:city, appid:this._appid, units:this._units}
        Object.keys(params).forEach(key => urlow.searchParams.append(key, params[key]))
        console.log(urlow);
        return fetch(urlow.toString())
        .then(x => x.json())
    }

    async getUser(name) {
        const urlow = new url.URL('https://dummyjson.com/users/search'),
        params = {q:name}
        Object.keys(params).forEach(key => urlow.searchParams.append(key, params[key]))
        const reponse = await fetch(urlow);
        return await reponse.json();
    }

    async getProduct(name) {
        const urlow = new url.URL('https://dummyjson.com/products/search'),
        params = {q:name}
        Object.keys(params).forEach(key => urlow.searchParams.append(key, params[key]))
        const reponse = await fetch(urlow);
        return await reponse.json();
    }
};

module.exports.OpenWeatherMap = OpenWeatherMap;

