const readline = require('readline');
let { OpenWeatherMap } = require('./moduleOpenWeather.js');
let ow = new OpenWeatherMap('30acb185764d2be34d5381c3280a605a','metric');
/* 'gambit' renvoie a la notion de tactique, d'entree en matiere, 
 * d'amorce de conversation 
 * il s'agit d'un tableau, contenant des object literaux.
 * chaque objet contient deux cles, 'trigger' correspondant 
 * a un expression reguliere
 * et output contenant la/les reponse(s) possible(s), ou une fonction 
 * dont le role est de retourner la reponse
 *
 *  @todo : ajouter plusieurs autres possibilites ( dont la date )
 *  @todo (avance): ajouter la possibilite de gerer des functions 
 *      dans les outputs :
 * 	 { 'trigger' : /(?:.*)temps a?)(?:.*)/,
 *	  'output'  : ['^getWeather']
 *	 },
 *  @todo (avanc"): trouver une API et l'interroger ( ex: meteo ). 
 *       Utiliser le module 'sync-request'. */
const gambits = [
	{ 'trigger' : /salut|bonjour|hello|hi\s*(.*)/,
	  'output'  : ['Salut!', 'Bonjour']
	},
	{ 'trigger' : /(comment\s?)?[c]a\sva/,
	  'output'  : ['Je vais bien merci']
	},
	{ 'trigger' : /aurevoir|a bientot|bye|quit|exit(.*)/,
	  'output'  : ['^exit']
	},
	{ 'trigger' : /date|today/,
	  'output'  : ['^getDate']
	},
	{ 'trigger' : /^jour|^day/,
	  'output'  : ['^getDay']
	},
	{ 'trigger' : /mois|moi|month/,
	  'output'  : ['^getMonth']
	},
	{ 'trigger' : /an|annee|année|year/,
	  'output'  : ['^getYear']
	},
	{ 'trigger' : /fullday/,
	  'output'  : ['^getFullDay']
	},
	{ 'trigger' : /(?:meteo|weather|temps?)\s(.+)/,
	  'output'  : ['^getWeather']
	},
	{ 'trigger' : /(?:user?)\s(.+)/,
	  'output'  : ['^getUser']
	},
	{ 'trigger' : /(?:product?)\s(.+)/,
	  'output'  : ['^getProduct']
	},
];
// Utilitaires
let utils = {
	
	random: function (min, max) {
  		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	getDate: function() {
		return new Date().toString();		
	},
	getDay: function() {
		let date = new Date();
		return days[date.getDay() - 1];
	},
	getMonth: function() {
		let date = new Date();
		return months[date.getDay()];
	},
	getYear: function() {
		let date = new Date();
		return date.getFullYear();
	},
	getFullDay: function() {
		let date = new Date();
		let numero = date.getDate();
		let jour = days[date.getDay() - 1];
		let mois = months[date.getMonth()];
		let year = date.getFullYear();
		return 'Nous sommes le ' + jour + ' ' + numero + ' ' + mois + ' ' + year;
	},
	getWeather: function(params) {
		let city = params[1]
		ow.getWeather(city)
			.then(meteo => {
				console.log(meteo);
			});
	},
	getUser: function(params) {
		let user = params[1];
		ow.getUser(user)
			.then(x => {
				console.log(x);
			});
	},
	exit: function() {
		process.exit();
	},
	getProduct: function(params) {
		let product = params[1];
		ow.getProduct(product)
			.then(x => {
				console.log(x);
			});
	}
}

let days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
let months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]

//notez que l'on specifie un stream en entree et en sortie.

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// ceci n'est pas obligatoire, permet de poser une question et 
// d'attendre une reponse de l'utilisateur
rl.question('Quel est votre prenom ? \n', function(answer) {
	console.log('Bienvenue ' + answer + ' ! \n');
});
// L'evenement 'ligne' est emis chaque fois que le flux d'entree recoit
// une entree de fin de ligne (\ n, \ r ou \ r \ n). 
// Cela se produit generalement lorsque l'utilisateur appuie sur 
//les touches <Entree> ou <Retour>.
rl.on('line', function(input){

	let found = false;
	let len = gambits.length;
// iteration sur chaque gambit afin de touver une correspondance 
// avec ce qui a ete saisi
	for(let i=0; i<len; i++)
	{
		let result = input.match(gambits[i].trigger);
		if(result != null) {
			// for (let j = 0; j < result.length; j++) {
			// 	console.log(result[j]);
			// }
			let index = utils.random(0, gambits[i]['output'].length - 1);
			let output = gambits[i]['output'][index];
			found = true;
			if (output[0] == '^') {
				let func = output.substring(1);
				console.log(utils[func](result));
			}
			else {
				console.log(output);
			}
		}
	}
	if(!found){
		console.log("je n'ai pas compris \n");
	}
});



