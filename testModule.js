let { OpenWeatherMap } = require('./moduleOpenWeather.js');
let ow = new OpenWeatherMap('30acb185764d2be34d5381c3280a605a','metric');
ow.getWeather('Orléans')
.then(x => {
    console.log(x);
})
.catch(err => {
    console.log('Error while retrieving API data');
});

ow.getUser('John')
.then(x => {
    console.log(x);
});

ow.getProduct('Laptop')
.then(x => {
    console.log(x);
});